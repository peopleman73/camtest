import flet as ft
import os
import cv2
import base64
from video_preview import VideoPreview
from image_viewer import ImageViewer

class ObjectA:
    def get_container(self, page):
        self.ip_field = ft.TextField(label="IP Address", value='169.254.8.165')
        self.user_field = ft.TextField(label="Login", value='root')
        self.password_field = ft.TextField(label="Password", value='Securosiss1!!')
        self.record_button = ft.ElevatedButton(text="Record", on_click=lambda e: self.check_camera(page))

        return ft.Container(
            content=ft.Row([
                self.ip_field,
                self.user_field,
                self.password_field,
                self.record_button,
            ]),
            alignment=ft.alignment.center,
            border=ft.border.all(1),
            padding=5,
            height=80,
        )

    def check_camera(self, page):
        ip = self.ip_field.value
        user = self.user_field.value
        password = self.password_field.value
        video_preview = VideoPreview(ip, user, password)
        dialog = ft.AlertDialog(
            modal=True,
            content=video_preview,
            actions=[],
            actions_alignment=ft.MainAxisAlignment.END,
        )
        page.dialog = dialog
        dialog.open = True
        page.update()

class ObjectB:
    def get_container(self, page, show_photos_callback):
        photo_cards = self.create_photo_cards(page, show_photos_callback)
        return ft.Container(
            content=ft.Column(photo_cards, scroll="auto"),
            alignment=ft.alignment.center,
            border=ft.border.all(1),
            padding=5,
            height=page.height - 80,
            width=200,
        )

    def create_photo_cards(self, page, show_photos_callback):
        records_folder = "Records"
        subfolders = [f.path for f in os.scandir(records_folder) if f.is_dir()]
        photo_cards = []

        for folder in subfolders:
            photo_files = [f for f in os.listdir(folder) if f.endswith(('.png', '.jpg', '.jpeg'))]
            if photo_files:
                first_photo_path = os.path.join(folder, photo_files[0])
                folder_name = os.path.basename(folder)

                card = ft.Card(
                    content=ft.Column([
                        ft.Image(src=first_photo_path, width=150, height=100),
                        ft.Text(folder_name),
                        ft.ElevatedButton(text="Show", on_click=lambda e, folder=folder: show_photos_callback(folder))
                    ]),
                    elevation=2,
                    shape=ft.RoundedRectangleBorder(radius=10),
                    margin=10
                )
                photo_cards.append(card)

        return photo_cards

class ObjectC:
    def __init__(self):
        self.container_c0 = ft.Container(
            content=ft.Column([], scroll="auto"),
            alignment=ft.alignment.center,
            border=ft.border.all(1),
            padding=5,
            width=250
        )

        self.container_c1 = ft.Container(
            content=ft.Column([], expand=1),
            alignment=ft.alignment.center,
            border=ft.border.all(1),
            padding=5,
            expand=1
        )

    def get_container(self, page):
        return ft.Row([
            self.container_c0,
            self.container_c1
        ], expand=1)

    def update_c0(self, folder):
        photo_files = [f for f in os.listdir(folder) if f.endswith(('.png', '.jpg', '.jpeg'))][:100]
        photo_cards = []

        for photo_file in photo_files:
            photo_path = os.path.join(folder, photo_file)
            card = ft.Card(
                content=ft.GestureDetector(
                    content=ft.Image(src=photo_path, width=150, height=100),
                    on_tap=lambda e, path=photo_path: self.show_photo_in_c1(path)
                ),
                elevation=2,
                shape=ft.RoundedRectangleBorder(radius=10),
                margin=10
            )
            photo_cards.append(card)

        self.container_c0.content = ft.Column(photo_cards, scroll="auto")
        self.container_c0.update()

    def show_photo_in_c1(self, photo_path):
        # Масштабирование изображения
        image_viewer = ImageViewer(photo_path)
        mask_image = image_viewer.draw_interface()
        _, buffer = cv2.imencode('.png', mask_image)
        mask_image_data = base64.b64encode(buffer).decode('utf-8')
        photo = ft.Image(src_base64=mask_image_data, fit=ft.ImageFit.CONTAIN)

        # Получение размеров контейнера C1
        container_width = self.container_c1.width or 800  # Заменить на реальные размеры контейнера C1
        container_height = self.container_c1.height or 600  # Заменить на реальные размеры контейнера C1

        # Вычисление масштабирования
        scale_width = container_width / image_viewer.original_width
        scale_height = container_height / image_viewer.original_height
        scale = min(scale_width, scale_height)

        photo.width = int(image_viewer.original_width * scale)
        photo.height = int(image_viewer.original_height * scale)

        photo_gesture = ft.GestureDetector(
            content=photo,
            on_tap=lambda e: image_viewer.show_image()
        )

        self.container_c1.content = ft.Container(
            content=photo_gesture,
            alignment=ft.alignment.center,
            height=self.container_c1.height,
            expand=1
        )
        self.container_c1.update()

class ObjectD:
    def get_container(self, page):
        return ft.Container(
            content=ft.Text("D"),
            alignment=ft.alignment.center,
            border=ft.border.all(1),
            padding=5,
            height=(page.height - 80) / 2
        )

class ObjectE:
    def get_container(self, page):
        return ft.Container(
            content=ft.Text("E"),
            alignment=ft.alignment.center,
            border=ft.border.all(1),
            padding=5,
            height=page.height - 80,
            width=200,
        )
