import flet as ft
from components import ObjectA, ObjectB, ObjectC, ObjectD, ObjectE
import nest_asyncio

nest_asyncio.apply()

def main(page: ft.Page):
    page.title = "ISS Camera App"
    page.window_width = 1600
    page.window_height = 860

    object_a = ObjectA().get_container(page)
    object_c_instance = ObjectC()
    object_c = object_c_instance.get_container(page)
    object_b = ObjectB().get_container(page, object_c_instance.update_c0)
    object_d = ObjectD().get_container(page)
    object_e = ObjectE().get_container(page)

    page.add(
        ft.Column([
            object_a,
            ft.Row([
                object_b,
                object_c,
                ft.Column([
                    object_d,
                    object_e,
                ], expand=1),
            ], expand=1)
        ], expand=1)
    )

ft.app(target=main)
