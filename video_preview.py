import flet as ft
import base64
import cv2
import threading
import queue
import os
from onvif import ONVIFCamera, ONVIFError
from datetime import datetime
import asyncio

# Обновленные пути к WSDL и XSD файлам
wsdl_dir = 'C:/wsdl'
schema_dir = 'C:/wsdl/schema'

class VideoPreview(ft.UserControl):
    def __init__(self, ip, user, password):
        super().__init__()
        self.ip = ip
        self.user = user
        self.password = password
        self.rtsp_uri, self.fps = self.get_rtsp_uri_and_fps()
        self.cap = None
        self.stop_stream = False
        self.recording = False
        self.video_queue = queue.Queue(maxsize=100)
        self.stream_thread = None
        self.record_thread = None
        self.video_writer = None

    def get_rtsp_uri_and_fps(self):
        onvif_port = 80
        rtsp_port = 554
        try:
            camera = ONVIFCamera(self.ip, onvif_port, self.user, self.password, wsdl_dir=wsdl_dir)
            media_service = camera.create_media_service()
            profiles = media_service.GetProfiles()
            profile = profiles[0]
            stream_uri = media_service.GetStreamUri({
                'StreamSetup': {
                    'Stream': 'RTP-Unicast',
                    'Transport': 'RTSP'
                },
                'ProfileToken': profile.token
            })
            rtsp_uri = stream_uri.Uri
            rtsp_uri = f"rtsp://{self.user}:{self.password}@{self.ip}:{rtsp_port}{rtsp_uri[rtsp_uri.find('/', 7):]}"

            video_encoder_configuration = profile.VideoEncoderConfiguration
            fps = video_encoder_configuration.RateControl.FrameRateLimit
            print(f"Полученное FPS: {fps}")  # Вывод FPS в консоль
            return rtsp_uri, fps
        except ONVIFError as e:
            print(f"Ошибка ONVIF: {e}")
        except Exception as e:
            print(f"Произошла ошибка: {e}")
        return None, 30  # Возвращаем стандартное значение 30 FPS, если не удалось получить

    def did_mount(self):
        self.stream_thread = threading.Thread(target=self.start_video_stream)
        self.stream_thread.start()

    def start_video_stream(self):
        self.cap = cv2.VideoCapture(self.rtsp_uri)
        if not self.cap.isOpened():
            print(f"Не удалось открыть поток: {self.rtsp_uri}")
            return

        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)

        while not self.stop_stream:
            ret, frame = self.cap.read()
            if not ret:
                print("Не удалось прочитать кадр.")
                break

            if self.recording:
                self.video_queue.put(frame)

            _, buffer = cv2.imencode('.jpg', frame)
            frame_b64 = base64.b64encode(buffer).decode()
            loop.run_until_complete(self.update_image(frame_b64))

        self.release_resources()

    async def update_image(self, frame_b64):
        self.video_image.src_base64 = frame_b64
        await self.video_image.update_async()

    def release_resources(self):
        if self.cap:
            self.cap.release()
        if self.video_writer:
            self.video_writer.release()
            self.video_writer = None
        self.cap = None

    def build(self):
        self.video_image = ft.Image(
            border_radius=ft.border_radius.all(20),
            width=800,
            height=600
        )
        self.record_button = ft.ElevatedButton(text="Start", on_click=self.start_recording)
        self.stop_button = ft.ElevatedButton(text="Stop", on_click=self.stop_recording)
        self.close_button = ft.ElevatedButton(text="Close", on_click=self.close_dialog)
        self.progress_bar = ft.ProgressBar(width=200, visible=False)

        return ft.Column([
            ft.Row([self.record_button, self.stop_button, self.close_button, self.progress_bar], alignment=ft.MainAxisAlignment.CENTER),
            self.video_image
        ])

    def start_recording(self, e):
        self.start_time = datetime.now()
        print(f"Start button pressed at: {self.start_time.strftime('%Y-%m-%d %H:%M:%S')}")
        self.record_button.bgcolor = ft.colors.GREEN
        self.record_button.update()
        self.recording = True
        self.record_thread = threading.Thread(target=self.save_video)
        self.record_thread.start()

    def stop_recording(self, e):
        self.stop_time = datetime.now()
        print(f"Stop button pressed at: {self.stop_time.strftime('%Y-%m-%d %H:%M:%S')}")
        self.recording = False
        self.progress_bar.visible = True
        self.page.update()

    def close_dialog(self, e):
        self.stop_stream = True
        self.release_resources()
        self.page.dialog.open = False
        self.page.update()

    def save_video(self):
        timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        temp_video_file = f"temp_{timestamp}.avi"
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        out = None

        while self.recording or not self.video_queue.empty():
            if not self.video_queue.empty():
                frame = self.video_queue.get()
                if out is None:
                    out = cv2.VideoWriter(temp_video_file, fourcc, self.fps, (frame.shape[1], frame.shape[0]))
                out.write(frame)

        if out:
            out.release()

        self.save_frames_from_video(temp_video_file)

    def save_frames_from_video(self, video_file):
        if not os.path.exists(video_file):
            print(f"Временный видеофайл не найден: {video_file}")
            return

        print(f"Сохранение кадров из видеофайла: {video_file}")

        timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        records_dir = "Records"
        if not os.path.exists(records_dir):
            os.makedirs(records_dir)

        save_dir = os.path.join(records_dir, timestamp)
        counter = 1
        while os.path.exists(save_dir):
            save_dir = os.path.join(records_dir, f"{timestamp}_{counter}")
            counter += 1

        os.makedirs(save_dir)

        cap = cv2.VideoCapture(video_file)
        frame_count = 0
        total_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        self.progress_bar.value = 0
        self.page.update()

        while cap.isOpened():
            ret, frame = cap.read()
            if not ret:
                break
            frame_filename = os.path.join(save_dir, f"frame_{frame_count:04d}.png")
            cv2.imwrite(frame_filename, frame)
            frame_count += 1
            print(f"Сохранен кадр: {frame_filename}")

            self.progress_bar.value = frame_count / total_frames
            self.page.update()

        cap.release()
        
        video_size = os.path.getsize(video_file)
        print(f"Размер временного видеофайла: {video_size} байт")

        if os.path.exists(video_file):
            os.remove(video_file)
            print(f"Временный видеофайл удален: {video_file}")

        self.page.dialog.open = False
        self.progress_bar.visible = False
        self.page.update()
