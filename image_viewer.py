import cv2
import numpy as np
import os

class ImageViewer:
    def __init__(self, image_path):
        self.image_path = image_path
        self.load_image()
        self.scale = 1.0
        self.pan_offset_x = 0
        self.pan_offset_y = 0
        self.is_panning = False
        self.pan_start_x = 0
        self.pan_start_y = 0
        self.window_width = int(0.8 * 1600)  # 80% ширины экрана
        self.window_height = int(0.8 * 860)  # 80% высоты экрана
        self.selected_checkbox = None
        self.horizontal_line_X1 = None
        self.horizontal_line_X2 = None
        self.horizontal_line_2px_near = None
        self.horizontal_line_2px_far = None
        self.perspective_line = []
        self.rect_start = None
        self.rect_end = None
        self.drawing_rect = False
        self.X_dist = 0
        self.aspect_ratio = 4.642

        # Проверка наличия файла mask.txt
        if os.path.exists("mask.txt"):
            self.load_mask()

    def load_image(self):
        self.image = cv2.imread(self.image_path)
        if self.image is None:
            print("Ошибка загрузки изображения")
            exit()
        self.original_height, self.original_width = self.image.shape[:2]
        self.image_resized = self.image.copy()

        # Уменьшение масштаба изображения в 1.5 раза
        self.image_resized = cv2.resize(self.image, (0, 0), fx=1/2.2, fy=1/2.2)
        self.original_height, self.original_width = self.image_resized.shape[:2]

    def update_image(self):
        new_size = (int(self.original_width * self.scale), int(self.original_height * self.scale))
        self.image_resized = cv2.resize(self.image, new_size, interpolation=cv2.INTER_NEAREST)

    def draw_interface(self):
        interface = np.zeros((self.window_height, self.window_width, 3), dtype=np.uint8)
        interface.fill(255)

        roi_x1 = max(0, -int(self.pan_offset_x))
        roi_y1 = max(0, -int(self.pan_offset_y))
        roi_x2 = min(self.image_resized.shape[1], int(interface.shape[1] - self.pan_offset_x))
        roi_y2 = min(self.image_resized.shape[0], int(interface.shape[0] - self.pan_offset_y))

        interface_y1 = max(0, int(self.pan_offset_y))
        interface_y2 = min(interface.shape[0], int(self.image_resized.shape[0] + self.pan_offset_y))
        interface_x1 = max(0, int(self.pan_offset_x))
        interface_x2 = min(interface.shape[1], int(self.image_resized.shape[1] + self.pan_offset_x))

        roi_height = roi_y2 - roi_y1
        roi_width = roi_x2 - roi_x1
        interface_height = interface_y2 - interface_y1
        interface_width = interface_x2 - interface_x1

        if roi_height > 0 and roi_width > 0 and interface_height > 0 and interface_width > 0:
            min_height = min(roi_height, interface_height)
            min_width = min(roi_width, interface_width)
            interface[interface_y1:interface_y1 + min_height, interface_x1:interface_x1 + min_width] = \
                self.image_resized[roi_y1:roi_y1 + min_height, roi_x1:roi_x1 + min_width]

        # Чекбоксы
        cv2.rectangle(interface, (10, interface.shape[0] - 150), (210, interface.shape[0] - 120), (0, 0, 255), 2)
        cv2.putText(interface, '2 pixel (near)', (20, interface.shape[0] - 130), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
        cv2.rectangle(interface, (10, interface.shape[0] - 120), (210, interface.shape[0] - 90), (255, 0, 0), 2)
        cv2.putText(interface, 'Horizontal Line X1 (near)', (20, interface.shape[0] - 100), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
        cv2.rectangle(interface, (10, interface.shape[0] - 90), (210, interface.shape[0] - 60), (255, 0, 0), 2)
        cv2.putText(interface, 'Horizontal Line X2 (far)', (20, interface.shape[0] - 70), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
        cv2.rectangle(interface, (10, interface.shape[0] - 60), (210, interface.shape[0] - 30), (0, 0, 255), 2)
        cv2.putText(interface, '2 pixel (far)', (20, interface.shape[0] - 40), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
        cv2.rectangle(interface, (10, interface.shape[0] - 30), (210, interface.shape[0]), (255, 0, 0), 2)
        cv2.putText(interface, 'Number Rectangle', (20, interface.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)

        cv2.rectangle(interface, (220, interface.shape[0] - 30), (310, interface.shape[0]), (255, 0, 0), 2)
        cv2.putText(interface, 'Save', (230, interface.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)

        cv2.putText(interface, f'X_dist: {self.X_dist}', (320, interface.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)

        if self.horizontal_line_X1 is not None:
            y = int(self.horizontal_line_X1 * self.scale + self.pan_offset_y)
            cv2.line(interface, (0, y), (interface.shape[1], y), (0, 255, 0), 2)
            cv2.putText(interface, f'X1 (near): {int(self.horizontal_line_X1)}', (10, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
        if self.horizontal_line_X2 is not None:
            y = int(self.horizontal_line_X2 * self.scale + self.pan_offset_y)
            cv2.line(interface, (0, y), (interface.shape[1], y), (0, 255, 0), 2)
            cv2.putText(interface, f'X2 (far): {int(self.horizontal_line_X2)}', (10, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
        if self.horizontal_line_2px_near is not None:
            y = int(self.horizontal_line_2px_near * self.scale + self.pan_offset_y)
            cv2.line(interface, (0, y), (interface.shape[1], y), (255, 0, 255), 2)
            cv2.putText(interface, f'2px (near): {int(self.horizontal_line_2px_near)}', (10, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 255), 2)
        if self.horizontal_line_2px_far is not None:
            y = int(self.horizontal_line_2px_far * self.scale + self.pan_offset_y)
            cv2.line(interface, (0, y), (interface.shape[1], y), (255, 0, 255), 2)
            cv2.putText(interface, f'2px (far): {int(self.horizontal_line_2px_far)}', (10, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 255), 2)
        if len(self.perspective_line) == 2:
            pt1 = (int(self.perspective_line[0][0] * self.scale + self.pan_offset_x), int(self.perspective_line[0][1] * self.scale + self.pan_offset_y))
            pt2 = (int(self.perspective_line[1][0] * self.scale + self.pan_offset_x), int(self.perspective_line[1][1] * self.scale + self.pan_offset_y))
            cv2.line(interface, pt1, pt2, (0, 255, 0), 2)
            cv2.putText(interface, 'Perspective Line', (10, pt1[1] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
        if self.rect_start is not None and self.rect_end is not None:
            pt1 = (int(self.rect_start[0] * self.scale + self.pan_offset_x), int(self.rect_start[1] * self.scale + self.pan_offset_y))
            width = (self.rect_end[0] - self.rect_start[0])
            height = width / self.aspect_ratio
            pt2 = (int(self.rect_start[0] * self.scale + width * self.scale + self.pan_offset_x), int(self.rect_start[1] * self.scale + height * self.scale + self.pan_offset_y))
            cv2.rectangle(interface, pt1, pt2, (0, 0, 255), 2)
            cv2.putText(interface, f'Width: {int(width)}px', (pt1[0], pt1[1] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
        
        return interface

    def mouse_callback(self, event, x, y, flags, param):
        if event == cv2.EVENT_LBUTTONDOWN:
            if 10 <= x <= 210 and self.window_height - 150 <= y <= self.window_height - 120:
                self.selected_checkbox = '2 pixel (near)'
            elif 10 <= x <= 210 and self.window_height - 120 <= y <= self.window_height - 90:
                self.selected_checkbox = 'Horizontal Line X1'
            elif 10 <= x <= 210 and self.window_height - 90 <= y <= self.window_height - 60:
                self.selected_checkbox = 'Horizontal Line X2'
            elif 10 <= x <= 210 and self.window_height - 60 <= y <= self.window_height - 30:
                self.selected_checkbox = '2 pixel (far)'
            elif 10 <= x <= 210 and self.window_height - 30 <= y <= self.window_height:
                self.selected_checkbox = 'Number Rectangle'
            elif 220 <= x <= 310 and self.window_height - 30 <= y <= self.window_height:
                self.save_parameters()
            else:
                if self.selected_checkbox == 'Horizontal Line X1':
                    self.horizontal_line_X1 = (y - self.pan_offset_y) / self.scale
                elif self.selected_checkbox == 'Horizontal Line X2':
                    self.horizontal_line_X2 = (y - self.pan_offset_y) / self.scale
                elif self.selected_checkbox == '2 pixel (near)':
                    self.horizontal_line_2px_near = (y - self.pan_offset_y) / self.scale
                elif self.selected_checkbox == '2 pixel (far)':
                    self.horizontal_line_2px_far = (y - self.pan_offset_y) / self.scale
                elif self.selected_checkbox == 'Perspective Line':
                    if len(self.perspective_line) < 2:
                        self.perspective_line.append(((x - self.pan_offset_x) / self.scale, (y - self.pan_offset_y) / self.scale))
                elif self.selected_checkbox == 'Number Rectangle':
                    self.drawing_rect = True
                    self.rect_start = ((x - self.pan_offset_x) / self.scale, (y - self.pan_offset_y) / self.scale)
                    self.rect_end = None
        elif event == cv2.EVENT_MOUSEMOVE:
            if self.drawing_rect and self.selected_checkbox == 'Number Rectangle':
                current_x = (x - self.pan_offset_x) / self.scale
                width = current_x - self.rect_start[0]
                height = width / self.aspect_ratio
                self.rect_end = (self.rect_start[0] + width, self.rect_start[1] + height)
            if self.is_panning:
                self.pan_offset_x += (x - self.pan_start_x)
                self.pan_offset_y += (y - self.pan_start_y)
                self.pan_start_x = x
                self.pan_start_y = y
        elif event == cv2.EVENT_LBUTTONUP:
            if self.drawing_rect and self.selected_checkbox == 'Number Rectangle':
                current_x = (x - self.pan_offset_x) / self.scale
                width = current_x - self.rect_start[0]
                height = width / self.aspect_ratio
                self.rect_end = (self.rect_start[0] + width, self.rect_start[1] + height)
                self.drawing_rect = False
        elif event == cv2.EVENT_RBUTTONDOWN:
            self.is_panning = True
            self.pan_start_x = x
            self.pan_start_y = y
        elif event == cv2.EVENT_RBUTTONUP:
            self.is_panning = False
        elif event == cv2.EVENT_MOUSEWHEEL:
            if flags > 0:
                new_scale = self.scale * 1.1
                scale_change = 1.1
            else:
                new_scale = self.scale * 0.9
                scale_change = 0.9

            if new_scale * self.original_width >= self.window_width:
                self.scale = new_scale

                if flags > 0:
                    self.pan_offset_x = (self.pan_offset_x - x) * scale_change + x
                    self.pan_offset_y = (self.pan_offset_y - y) * scale_change + y
                else:
                    if self.scale * self.original_width < self.window_width:
                        self.scale = self.window_width / self.original_width
                    else:
                        self.pan_offset_x = (self.pan_offset_x - x) * scale_change + x
                        self.pan_offset_y = (self.pan_offset_y - y) * scale_change + y

                    if self.pan_offset_x > 0:
                        self.pan_offset_x = 0
                    if self.pan_offset_x + int(self.original_width * self.scale) < self.window_width:
                        self.pan_offset_x = self.window_width - int(self.original_width * self.scale)
                    if self.pan_offset_y > 0:
                        self.pan_offset_y = 0
                    if self.pan_offset_y + int(self.original_height * self.scale) < self.window_height:
                        self.pan_offset_y = self.window_height - int(self.original_height * self.scale)

                self.update_image()

    def save_parameters(self):
        params = {
            "horizontal_line_X1": self.horizontal_line_X1,
            "horizontal_line_X2": self.horizontal_line_X2,
            "horizontal_line_2px_near": self.horizontal_line_2px_near,
            "horizontal_line_2px_far": self.horizontal_line_2px_far,
            "perspective_line": self.perspective_line,
            "rect_start": self.rect_start,
            "rect_end": self.rect_end,
            "aspect_ratio": self.aspect_ratio,
        }
        with open("mask.txt", "w") as f:
            for key, value in params.items():
                f.write(f"{key}: {value}\n")
        print("Parameters saved to mask.txt")

    def load_mask(self):
        with open("mask.txt", "r") as f:
            lines = f.readlines()
        params = {}
        for line in lines:
            key, value = line.strip().split(": ")
            if key in ["horizontal_line_X1", "horizontal_line_X2", "horizontal_line_2px_near", "horizontal_line_2px_far"]:
                try:
                    params[key] = float(value)
                except ValueError:
                    params[key] = None
            elif key == "aspect_ratio":
                params[key] = float(value)
            elif key in ["rect_start", "rect_end"]:
                params[key] = eval(value)
            elif key == "perspective_line":
                try:
                    params[key] = [tuple(map(float, x.strip("()").split(", "))) for x in value.strip("[]").split("), (")]
                except ValueError:
                    params[key] = []

        self.horizontal_line_X1 = params.get("horizontal_line_X1", None)
        self.horizontal_line_X2 = params.get("horizontal_line_X2", None)
        self.horizontal_line_2px_near = params.get("horizontal_line_2px_near", None)
        self.horizontal_line_2px_far = params.get("horizontal_line_2px_far", None)
        self.perspective_line = params.get("perspective_line", [])
        self.rect_start = params.get("rect_start", None)
        self.rect_end = params.get("rect_end", None)
        self.aspect_ratio = params.get("aspect_ratio", 4.642)

    def show_image(self):
        cv2.namedWindow('Image', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('Image', self.window_width, self.window_height)
        cv2.setMouseCallback('Image', self.mouse_callback)

        while True:
            interface = self.draw_interface()
            cv2.imshow('Image', interface)
            
            key = cv2.waitKey(1) & 0xFF
            if key == 27:  # Нажатие ESC для выхода
                break
            elif key == ord('r'):  # Сброс всех линий и прямоугольников
                self.horizontal_line_X1 = None
                self.horizontal_line_X2 = None
                self.horizontal_line_2px_near = None
                self.horizontal_line_2px_far = None
                self.perspective_line = []
                self.rect_start = None
                self.rect_end = None

        cv2.destroyAllWindows()
